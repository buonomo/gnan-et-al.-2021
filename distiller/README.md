This folder contains the project.yml file for distiller, a nextflow pipeline
used in the Gnan et al. paper for Hi-C processing from fastq to cooler files.

The distiller pipeline is available from https://github.com/open2c/distiller-nf