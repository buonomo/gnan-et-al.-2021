library(ggplot2)
library(scales)
library(tidyverse)
library(Cairo)
library(foreach)
library(DescTools)

#set grafical info
CairoFonts(
  regular="Helvetica:style=Regular",
  bold="Helvetica:style=Bold",
  italic="Helvetica:style=Italic",
  bolditalic="Helvetica:style=Bold Italic,bold",
  symbol="Symbol"
)

# load 1kb data
dir='~/OneDrive - University of Edinburgh/Review_paper/RT_bedgraph_RPM/'
files=list.files(dir,pattern = '1kb')
data=foreach(file=files,.combine = 'rbind',.packages = 'tidyverse')%do%{
  
  read_tsv(paste0(dir,file), col_names = F)%>%
    mutate(line=file) %>%
    # filter region of interest
    dplyr::filter(X1 == 'chr17',
                  X2 >= 50000000,
                  X3 <= 80000000)
}


p=data %>%
  #rename samples and reshape data
  mutate( line=case_when(
    line %like% '5_%' ~ 'Rif1 KO 1',
    line %like% '18_%' ~ 'Rif1 KO 2',
    line %like% '24_%' ~ 'Rif1 KO 3',
    line %like% 'B_%' ~ 'Rif1 WT 1',
    line %like% 'F_%' ~ 'Rif1 WT 2',
    line %like% 'H_%' ~ 'Rif1 WT 3',
    line %like% 'A7_%' ~ 'Rif1 tgWT 1',
    line %like% 'H4_%' ~ 'Rif1 tgWT 2',
    line %like% 'H6_%' ~ 'Rif1 tgWT 3',
    line %like% 'G11_%' ~ 'Rif1 ΔPP1 1',
    line %like% 'H1_%' ~ 'Rif1 ΔPP1 2',
    line %like% 'H2_%' ~ 'Rif1 ΔPP1 3',  
    line %like% 'F14_%' ~ 'Rif1 Het',  
    T~line
  ))%>%
  mutate(
    gen=str_remove(line,' [123]$'),
    gen=factor(gen, levels = c('Rif1 WT','Rif1 KO','Rif1 ΔPP1','Rif1 tgWT','Rif1 Het')))%>%
  group_by(X1, X2, X3, gen) %>%
  #calculate mean and sd
  summarise(RT = mean(X4),
            sd = sd(X4)) %>%
  #plot
  mutate(
    sd = ifelse(gen == 'Rif1 Het', 0, sd),
    min = RT - sd,
    max = RT + sd
  ) %>%
  ggplot(aes(x = as.numeric(X3), y = RT, col = gen)) + geom_hline(yintercept = 0) +
  geom_line(size = 0.8) + scale_x_continuous(labels = trans_format(
    format = 'Mb',
    trans = function(x)
      paste((x / 1000000), 'Mb', sep = '')
  ))+
  geom_ribbon(aes(
    ymin = min ,
    ymax = max,
    fill = gen,
    group = gen,
    col = NULL
  ), alpha = 0.3)+
  
  scale_color_manual(values = c("#636363","#BDBDBD","#9ECAE1","#3182BD",'darkblue'))+
  scale_fill_manual(values = c("#636363","#BDBDBD","#9ECAE1","#3182BD",'darkblue'))+
  
  annotate('Text',y=0,x=Inf,label='bold("Early")',hjust=1,vjust=-0.25, parse = TRUE, size=3)+
  annotate('Text',y=0,x=Inf,label='bold("Late ")',hjust=1,vjust=1.25, parse = TRUE, size=3)+
  
  ylab(expression(bold('RT '~log[2]~over('Early','Late'))))+xlab('')+facet_grid(gen~X1)+ theme(panel.grid.major =  element_line(colour = "grey"), panel.grid.minor= element_blank(),
                                                                                                 panel.background = element_blank(), axis.line = element_line(colour = "black"), legend.text = element_text(size=14, face='bold'),
                                                                                                 legend.title=element_blank(),axis.text.x = element_text(size = 14,face="bold",hjust = 1),axis.text.y = element_text(size = 14,face="bold"),
                                                                                                 axis.title.y =element_text(size = 18, face="bold"), legend.position = 'top',strip.text.x = element_text(size = 14, face="bold"), strip.text.y =element_blank() )


CairoPDF( '~/OneDrive - University of Edinburgh/PLOTS/FigS3B.pdf',width = 45/2.54, height = 30/2.54)
p

dev.off()
