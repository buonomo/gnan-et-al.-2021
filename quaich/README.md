This folder contains the config file used for analysis of Hi-C data in the
Gnan et al paper. It is used in conjunction with quaich, a snakemake pipeline
for Hi-C analysis (https://github.com/open2c/quaich).

Quaich v0.1.2 (doi 10.5281/zenodo.4613910) was used with these config files. Make sure to use the quaich
conda environment for quaich (comes with it), and the notebook environment for
the jupyter notebook.